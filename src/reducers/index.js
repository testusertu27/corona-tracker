import { combineReducers } from 'redux';
import CoronaDataReducer from './coronaDataReducer';


export default combineReducers({
  corona: CoronaDataReducer
})