import { 
  GET_WORLD_DATA,
  SEARCH_COUNTRY_DATA,
  SEARCH_COUNTRY_DATA_ERROR,
  GET_ALL_COUNTRY_DATA
} from "../actions/types"

const initialState = {
  allData: null,
  countryData: null,
  allcountries: null
}

const CoronaDataReducer = (state = initialState, action) => {
  switch(action.type){
    case GET_WORLD_DATA:
      return {
        ...state,
        allData: action.payload
      }
    case SEARCH_COUNTRY_DATA:
      return {
        ...state,
        countryData: action.payload
      }
    case SEARCH_COUNTRY_DATA_ERROR:
      return {
        ...state,
        countryData: null
      }
    case GET_ALL_COUNTRY_DATA:
      return {
        ...state,
        allcountries: action.payload
      }
    case "For_EMPTY_SEARCH":
      return {
        ...state,
        countryData: null
      }
    default:
      return state
  }
}

export default CoronaDataReducer;