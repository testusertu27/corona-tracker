import { 
  SEARCH_COUNTRY_DATA,
  SEARCH_COUNTRY_DATA_ERROR,
  GET_WORLD_DATA,
  GET_ALL_COUNTRY_DATA
} from './types';
import axios from 'axios';

export const searchCountry = (country) => {
  return (
    dispatch => {
      if(country === ""){
        dispatch({
          type: "For_EMPTY_SEARCH"
        })
      }else{
        axios.get('https://corona.lmao.ninja/countries/' + country)
          .then((res) => {
            dispatch({
              type: SEARCH_COUNTRY_DATA,
              payload: res.data
            })
          })
          .catch((error) => {
            dispatch({
              type: SEARCH_COUNTRY_DATA_ERROR
            })
          })
      }  
    }
  )
}

export const worldData = () => {
  return (
    dispatch => {
      axios.get('https://corona.lmao.ninja/all')
      .then((res) => {
        dispatch({
          type: GET_WORLD_DATA,
          payload: res.data
        })
      })
      .catch(error => console.log('data not found'))
    }
  )
}

export const allCountryData = () => {
  return (
    dispatch => {
      axios.get('https://corona.lmao.ninja/countries')
      .then((res) => {
        dispatch({
          type: GET_ALL_COUNTRY_DATA,
          payload: res.data
        })
      })
    }
  )
}