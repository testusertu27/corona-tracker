import React from 'react';
import './App.css';

import Header from './components/header/Header';
import Showcase from './components/showcase/Showcase';
import CountryList from './components/countrylist/CountryList';
import Footer from './components/Footer';

function App() {
  return (
    <div>
      <Header />
      <Showcase />
      <CountryList />
      <Footer />
    </div>
  );
}

export default App;
