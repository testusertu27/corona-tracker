import React, { Component } from 'react';
import './showcase.css';
import { connect } from 'react-redux';
import { worldData } from '../../actions/coronaDataAction';

class Showcase extends Component {

  componentDidMount(){
    this.props.worldData()
  }

  render() {
    const { allData, countryData } = this.props
    return (
      <div className="showcase-container">

        <div className="country-name-flag">
          <div className="country-flag">
            <img 
              src={
                countryData === null ? 
                  "/images/virus.png" 
                : 
                countryData.countryInfo.flag
              } 
              alt="country-flag"/>
          </div>
          <div className="country-name">
            { 
              countryData === null ? "worldwide"
              : countryData.country
            }
          </div>
        </div>

        <div className="confirmed-cases">
          <div> confirmed: </div>  
          <div className="confirmed-number">
            { 
              countryData === null ? allData && allData.cases
              : countryData.cases
            }
          </div>
        </div>

        <div className="recovered-cases">
          <div>recovered: </div>
          <div className="recovered-number">
            {
              countryData === null ? allData && allData.recovered
                : countryData.recovered
            }
          </div> 
        </div>

        <div className="death-cases">
          <div>deaths:</div>
          <div className="deaths-number">
            {
              countryData === null ? allData && allData.deaths
                : countryData.deaths
            }
          </div> 
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  allData: state.corona.allData,
  countryData: state.corona.countryData
})

const mapDispatchToProps = (dispatch) => ({
  worldData: () => dispatch(worldData())
})

export default connect(mapStateToProps, mapDispatchToProps)(Showcase);