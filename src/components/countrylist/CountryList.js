import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import './countrylist.css';

import {allCountryData} from '../../actions/coronaDataAction';

const CountryList = (props) => {

  useEffect(() => {
    props.allCountryData()
  },[]);

  if(props.allcountries !== null){
    return (
      <div className="country-list-container">
        <div className="countrylist-table">
          <table>
            <tr>
              <th></th>
              <th>country</th>
              <th>total cases</th>
              <th>active cases</th>
              <th>total recovered</th>
              <th>total deaths</th>
            </tr>
            {
              props.allcountries.map((country, key) => (
                <tr>
                  <td>
                    <img 
                      src={country.countryInfo.flag} 
                      alt="country-flag" 
                      style={{height: "20px", width:"25px"}}/>
                  </td>
                  <td>{country.country}</td>
                  <td>{country.cases}</td>
                  <td>{country.active}</td>
                  <td>{country.recovered}</td>
                  <td>{country.deaths}</td>
                </tr>
              ))
            }
          </table>
        </div>
      </div>
    );
  }else{
    return null
  }
};

const mapStateToProps = (state) => ({
  allcountries: state.corona.allcountries
})

const mapDispatchToProps = (dispatch) => ({
  allCountryData: () => dispatch(allCountryData())
})

export default connect(mapStateToProps, mapDispatchToProps)(CountryList);