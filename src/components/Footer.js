import React from 'react';

const Footer = () => {
  return (
    <div 
      className="footer-container" 
      style={{
        height: "35px", 
        textAlign:"center", 
        paddingTop: "8px", 
        backgroundColor: " #555555",
        color:"#ffffff"
      }}
    >
      created by : Bishal Tako
    </div>
  );
};

export default Footer;