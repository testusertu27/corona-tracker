import React, { Component } from 'react';
import { debounce } from 'lodash';
import { connect } from 'react-redux';
import './header.css'

import { searchCountry } from '../../actions/coronaDataAction';

class Header extends Component {

  state = {
    countryName: ''
  }

  OnSearch = debounce((text) => {
    this.setState({
      countryName: text
    })
    /* return this.state.countryName === "" ?  null : this.props.searchCountry(this.state.countryName.toLowerCase()) */
    this.props.searchCountry(this.state.countryName.toLowerCase())
  },1000)

  render() {
    return (
      <div className="header-container">
        <div className="logo">
          <img src="/images/index.png" alt=""/>
          <h3>COVID-19</h3>
        </div>
        <div className="search-bar">
          <input 
            type="text" 
            placeholder="Search a country" 
            onChange={(e) => this.OnSearch(e.target.value)}
          />
        </div>
        <div className="map-link">
          <a href="https://www.google.com/covid19-map/" target="_blank">Google live map</a>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  searchCountry: (countryName) => dispatch(searchCountry(countryName))
})

export default connect(null, mapDispatchToProps)(Header);